#include "elevatorobserver.h"
#include <iostream>

using namespace std;

void ElevatorObserver::eventDoorOpened() {
    cout << "elevator opened the doors..." << endl;
}
void ElevatorObserver::eventDoorClosed() {
    cout << "elevator shut the doors..." << endl;
}

void ElevatorObserver::eventFloorMoving(const int floor) {
    cout << "elevator passes the " << floor << " floor..." << endl;
}

void ElevatorObserver::eventArrivedFloor(const int floor) {
    cout << "elevator arrived the " << floor << " floor" << endl;
}
