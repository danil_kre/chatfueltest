#ifndef ELEVATOR_H
#define ELEVATOR_H

#include <atomic>
#include "ielevator.h"
#include <list>
#include <mutex>
#include <condition_variable>

class Elevator: public IElevator
{
public:
    Elevator();

    enum class DIRECTION {
        UP,
        DOWN
    };

    // IElevator implementation
    STATE getState() const override;
    void pushedFloorButton(const SIDE side, const int pos) override;
    void setObserver(IElevatorObserver *observer) override;
    void setDoorsTimeout(const int timeout) override;
    void setSpeed(const float speed) override;

    // Own methods

    /**
     * @brief getPosition
     * @return current floor
     */
    int getPosition() const;
    /**
     * @brief setDestination - setup destination floor
     * @param pos - value of destination floor
     */
    void setDestination(const int pos);
    /**
     * @brief getDestination
     * @return destination floor
     */
    int getDestination() const;

    /**
     * @brief execTask
     * Start go to destination floor
     */
    void execTask();

    /**
     * @brief close
     * Function close the doors
     */
    void close();
    /**
     * @brief open
     * Function open the doors
     */
    void open();

    /**
     * @brief move - Moving elevator to 1 floor
     * @param where - moving direction (up/down)
     */
    void move(const DIRECTION where);

    void incrementPos();
    void decrementPos();

    /**
     * @brief getDoorsTimeout
     * @return keep doors opened timeout
     */
    int getDoorsTimeout() const;


protected:

    friend void waitElev(Elevator *elevator);
    std::mutex doorsCondvarMutex;
    std::condition_variable doorsCondvar;

    // Current floor
    int current_floor = 1;

    // Where we need to go
    int destination = -1;

    // Current state of doors
    STATE mState = STATE::CLOSED;

    // Timeout between opening and closig doors id idle state
    int keep_opened_doors_timeout = 10;

    // Elevator speed (floors per sec)
    float speed = 1;

    // Observer
    IElevatorObserver *observer = nullptr;
};

#endif // ELEVATOR_H
