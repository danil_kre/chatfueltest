class IElevatorObserver;
class IElevator {
public:
    /**
     * @brief The STATE enum
     */
    enum class STATE {
        CLOSED = 0,
        OPENED,
        MOVING
    };

    /**
     * @brief The SIDE enum
     * Where button was pushed
     */
    enum class SIDE {
        INSIDE, // the button was pressed inside the elevator
        OUTSIDE // the button was pressed outside the elevator
    };

    virtual ~IElevator() {}

    virtual STATE getState() const = 0;
    virtual void pushedFloorButton(const SIDE side, const int pos) = 0;
    virtual void setObserver(IElevatorObserver *observer) = 0;
    virtual void setDoorsTimeout(const int timeout) = 0;
    virtual void setSpeed(const float speed) = 0;
};
