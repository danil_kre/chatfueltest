#include "elevator.h"
#include <iostream>
#include <chrono>
#include <thread>
#include <ielevatorobserver.h>

using namespace std;

void waitElev(Elevator *elevator) {
    std::unique_lock<std::mutex> lock(elevator->doorsCondvarMutex);
    auto res = elevator->doorsCondvar.wait_for(lock, std::chrono::seconds(elevator->getDoorsTimeout()));
    if (res == cv_status::timeout)
        elevator->close();
}

Elevator::Elevator() {

}

IElevator::STATE Elevator::getState() const {
    return this->mState;
}

int Elevator::getPosition() const {
    return current_floor;
}

void Elevator::setDestination(const int pos) {
    this->destination = pos;
}

int Elevator::getDestination() const {
    return this->destination;
}

void Elevator::pushedFloorButton(const SIDE side, const int pos) {
    if (getPosition() == pos) {
        this->open();
        std::thread closeDoorThread(waitElev, this);
        closeDoorThread.detach();
        return;
    }

    switch (this->getState()) {
    case STATE::CLOSED:
        this->setDestination(pos);
        execTask();
        break;
    case STATE::OPENED:
        if (side == SIDE::INSIDE)
        {
            this->doorsCondvar.notify_all();
            this->setDestination(pos);
            execTask();
        }
        break;
    case STATE::MOVING:
        break;
    }
}

void Elevator::execTask() {
    this->close();
    while (getPosition()!= getDestination()) {
        this->move(getPosition() < getDestination() ?
                    DIRECTION::UP :
                    DIRECTION::DOWN);
    }

    if (observer)
        observer->eventArrivedFloor(this->getPosition());

    this->open();
    std::thread closeDoorThread(waitElev, this);
    closeDoorThread.detach();
}

void Elevator::setObserver(IElevatorObserver *observer) {
    this->observer = observer;
}

void Elevator::setDoorsTimeout(const int timeout) {
    this->keep_opened_doors_timeout = timeout;
}

void Elevator::setSpeed(const float speed) {
    this->speed = speed;
}

void Elevator::close() {
    if (this->getState() == STATE::CLOSED)
        return;
    this->mState = STATE::CLOSED;

    if (observer)
        observer->eventDoorClosed();
}

void Elevator::open() {
    if (this->getState() == STATE::OPENED)
        return;
    this->mState = STATE::OPENED;
    if (observer)
        observer->eventDoorOpened();
}

void Elevator::move(const DIRECTION where) {
    this->mState = STATE::MOVING;
    if (observer)
        observer->eventFloorMoving(this->getPosition());

    std::this_thread::sleep_for(std::chrono::milliseconds(static_cast<int>(1000/this->speed)));
    if (where == DIRECTION::UP)
        this->incrementPos();
    else
        this->decrementPos();
}

void Elevator::incrementPos() {
    current_floor++;
}

void Elevator::decrementPos() {
    current_floor--;
}

int Elevator::getDoorsTimeout() const {
    return this->keep_opened_doors_timeout;
}
