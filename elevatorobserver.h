#ifndef ELEVATOROBSERVER_H
#define ELEVATOROBSERVER_H

#include "ielevatorobserver.h"

class ElevatorObserver:public IElevatorObserver
{
public:
    ElevatorObserver() = default;

    //IElevatorObserver impl
    void eventDoorOpened() override;
    void eventDoorClosed() override;
    void eventFloorMoving(const int floor) override;
    void eventArrivedFloor(const int floor) override;
};

#endif // ELEVATOROBSERVER_H
