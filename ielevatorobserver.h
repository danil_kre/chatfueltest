#ifndef IELEVATOROBSERVER_H
#define IELEVATOROBSERVER_H

class IElevatorObserver {
public:
    virtual ~IElevatorObserver() {}

    virtual void eventDoorOpened() = 0;
    virtual void eventDoorClosed() = 0;
    virtual void eventFloorMoving(const int floor) = 0;
    virtual void eventArrivedFloor(const int floor) = 0;
};

#endif // IELEVATOROBSERVER_H
