#include <iostream>
#include "elevator.h"
#include "elevatorobserver.h"
#include <memory>
using namespace std;

constexpr auto EXIT_COMMAND = "exit";

int main(int argc, char *argv[])
{
    if (argc < 4)
    {
        cout << "Wrong number of parameters" << endl;
        return 0;
    }

    const int FloorCount = atoi(argv[1]);
    if (FloorCount < 5 || FloorCount > 20)
    {
        cout << "Wrong count of floors" << endl;
        return 0;
    }

    const int FloorHeight = atoi(argv[2]);
    if (FloorHeight < 1)
    {
        cout << "Wrong height of the floor" << endl;
        return 0;
    }

    const int ElevatorSpeed = atoi(argv[3]);
    if (ElevatorSpeed < 0)
    {
        cout << "Wrong elevator speed" << endl;
        return 0;
    }

    const int DoorsTimeout = atoi(argv[4]);
    if (DoorsTimeout < 0)
    {
        cout << "Wrong opened doors timeout" << endl;
        return 0;
    }

    std::unique_ptr<IElevator> elevator(new Elevator());
    elevator->setSpeed(static_cast<float>(FloorHeight)/ElevatorSpeed);
    std::unique_ptr<ElevatorObserver> observer(new ElevatorObserver());
    elevator->setObserver(observer.get());

    cout << "Enter commands: " << endl <<
            " i<floor> - enter button inside elevator" << endl <<
            " e<floor> - enter button outside elevator" << endl <<
            " exit     - finish" << endl;

    while (true) {
        std::string userInput;

        cin >> userInput;
        if (userInput == EXIT_COMMAND) {
            break;
        }
        if (userInput[0] == 'i') {
            int floor = std::atoi(userInput.c_str()+1);
            if (floor > 0 && floor <= FloorCount)
                elevator->pushedFloorButton(IElevator::SIDE::INSIDE, floor);
        } else if (userInput[0] == 'e') {
            int floor = std::atoi(userInput.c_str()+1);
            if (floor > 0 && floor <= FloorCount)
                elevator->pushedFloorButton(IElevator::SIDE::OUTSIDE, floor);
        } else {
            cout << "Unknown command" << endl;
        }
    }

    cout << " --- END ---" << endl;
    return 0;
}
