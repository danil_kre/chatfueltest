TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.cpp \
    elevator.cpp \
    elevatorobserver.cpp
HEADERS += ielevator.h \
    elevator.h \
    elevatorobserver.h \
    ielevatorobserver.h
